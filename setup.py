#!/usr/bin/env python
import re
import subprocess
import sys
import os
import six

from distutils.core import setup
from setuptools import find_packages
from setuptools.command.install_lib import install_lib as _install_lib
from distutils.command.build import build as _build
from distutils.command.sdist import sdist
from distutils.cmd import Command


class compile_translations(Command):
    description = 'compile message catalogs to MO files via django \
compilemessages'
    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        try:
            from django.core.management import call_command
            for dir in ('django_tables2',):
                for path, dirs, files in os.walk(dir):
                    if 'locale' not in dirs:
                        continue
                    curdir = os.getcwd()
                    os.chdir(os.path.realpath(path))
                    call_command('compilemessages')
                    os.chdir(curdir)
        except ImportError:
            six.print_('!!! Please install Django >= 1.4 to build '
                             'translations')


class build(_build):
    sub_commands = [('compile_translations', None)] + _build.sub_commands


class eo_sdist(sdist):

    def run(self):
        six.print_("creating VERSION file")
        if os.path.exists('VERSION'):
            os.remove('VERSION')
        version = get_version()
        version_file = open('VERSION', 'w')
        version_file.write(version)
        version_file.close()
        sdist.run(self)
        six.print_("removing VERSION file")
        if os.path.exists('VERSION'):
            os.remove('VERSION')


class install_lib(_install_lib):
    def run(self):
        self.run_command('compile_translations')
        _install_lib.run(self)


def get_version():
    '''Use the VERSION, if absent generates a version with git describe, if not
       tag exists, take 0.0.0- and add the length of the commit log.
    '''
    if os.path.exists('VERSION'):
        with open('VERSION', 'r') as v:
            return v.read()
    if os.path.exists('.git'):
        p = subprocess.Popen(['git', 'describe', '--tags', '--dirty',
                              '--match=v*'], stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE)
        result = p.communicate()[0]
        if p.returncode == 0:
            return result.split()[0][1:].replace('-', '.')
        else:
            return '0.0.0-%s' % len(
                subprocess.check_output(['git', 'rev-list',
                                         'HEAD']).splitlines())
    return '0.0.0'


setup(
    name='django-tables2',
    version=get_version(),
    description='Table/data-grid framework for Django',

    author='Bradley Ayers',
    author_email='bradley.ayers@gmail.com',
    license='Simplified BSD',
    url='https://github.com/bradleyayers/django-tables2/',

    packages=find_packages(exclude=['tests.*', 'tests', 'example.*', 'example']),
    include_package_data=True,  # declarations in MANIFEST.in

    install_requires=['Django >=1.2', 'six'],

    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Software Development :: Libraries',
    ],
    cmdclass={'build': build, 'install_lib': install_lib,
              'compile_translations': compile_translations, 'sdist':
              eo_sdist},
)
